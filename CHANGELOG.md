# Versions

## [1.4.0] April 9, 2024

* Upgrade dependencies

## [1.3.1] September 11, 2023

* small fixes.

## [1.3.0] September 11, 2023

* add new features and extensions.
* refactoring.

## [1.2.1] September 9, 2023

* fix FlexText widget styles.

## [1.2.0] September 9, 2023

* upgrade dependencies.
* up dart and flutter version.

## [1.1.1] January 12, 2023

* upgrade dependencies.

## [1.1.0] January 10, 2023

* upgrade dependencies.

## [1.0.1] May 30, 2021

* upgrade dependencies.

## [1.0.0] Marth 19, 2021

* adding Null safety and extentions.

## [0.1.6] Febrary 18, 2021

* upgrade dependencies.

## [0.1.5] July 16, 2020

* adding TVDeviceType.

## [0.1.4] July 14, 2020

* fix device type for Amazon devices.

## [0.1.3] July 13, 2020

* fix device type for Fire Stick.

## [0.1.2] July 3, 2020

* fix device type when IOS.

## [0.1.1] April 2, 2020

* adding new feature.

## [0.1.0+1] January 20, 2020

* fix getValueByScreen.

## [0.1.0] January 20, 2020

* refactoring.

## [0.0.3] January 17, 2020

* added new features.

## [0.0.2] January 13, 2020

* added a FlexText widget.

## [0.0.1+1] January 13, 2020

* small fixes of description.

## [0.0.1] January 13, 2020

* init project!
